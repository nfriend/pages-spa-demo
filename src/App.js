import "./App.css";

function App() {
  const path = window.location.pathname.replace(/^\/pages-spa-demo/i, "");

  return (
    <div className="App">
      <header className="App-header">
        <h1>
          Hey there <span class="wave">👋</span>
        </h1>
        <p>
          You just navigated to <code>{path}</code>!
        </p>
        <p>
          But thanks to this project's{" "}
          <a
            className="App-link"
            href="https://gitlab.com/nfriend/pages-spa-demo/-/blob/main/public/_redirects"
            target="_blank"
            rel="noopener noreferrer"
          >
            <code>_redirects</code>
          </a>{" "}
          file, <code>index.html</code> was served instead!
        </p>
        <p>
          Check out the{" "}
          <a
            className="App-link"
            href="https://gitlab.com/nfriend/pages-spa-demo/"
            target="_blank"
            rel="noopener noreferrer"
          >
            source code.
          </a>
        </p>
      </header>
    </div>
  );
}

export default App;
