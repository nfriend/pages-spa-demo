# Pages client-side routing SPA demo

An example project that uses [`_redirects`](https://docs.gitlab.com/ee/user/project/pages/redirects.html) rules to implement client-side SPA routing.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Demo

View the live demo here: https://nfriend.gitlab.io/pages-spa-demo/a/client/side/route

You can replace `/a/client/side/route` with anything; the app's `index.html` will always be served thanks to this project's [`_redirects` file](public/_redirects).

## Implementation

This GitLab Pages feature was implemented in https://gitlab.com/gitlab-org/gitlab-pages/-/merge_requests/458.

Any feedback on this feature? Feel free to comment in [https://gitlab.com/gitlab-org/gitlab-pages/-/issues/500](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/500)!

## Developing

- Clone this repository
- Run `yarn`
- Run `yarn start`
